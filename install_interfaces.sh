#!/bin/bash

# This script install the interfaces used by LEAP client into the local MAVEN repository
# Usage: install_interfaces <version>

# create JAR file 
jar cvf edge-interfaces-$1.jar -C edge/target/classes unife/edge/EDGEStat.class
cd edgeRemote/target/classes
jar uf ../../../edge-interfaces-$1.jar unife/edgeRemote/EDGERemote*.class unife/edgeRemote/EDGERemoteServer.class unife/edgeRemote/exceptions/ unife/rmi/communication/RMIInputStream.class unife/rmi/communication/RMIOutputStream.class

cd ../../..
jar cvf edge-interfaces-$1-sources.jar -C edge/src/main/java unife/edge/EDGEStat.java
cd edgeRemote/src/main/java
jar uf ../../../../edge-interfaces-$1-sources.jar unife/edgeRemote/EDGERemote.java unife/edgeRemote/EDGERemoteServer.java unife/edgeRemote/exceptions/ unife/rmi/communication/RMIInputStream.java unife/rmi/communication/RMIOutputStream.java

cd ../../../..
jar cvf edge-interfaces-$1-javadoc.jar -C edge/target/site/apidocs unife/edge/EDGEStat.html
cd edgeRemote/target/site/apidocs 
jar uf ../../../../edge-interfaces-$1-sources.jar unife/edgeRemote/EDGERemote.html unife/edgeRemote/EDGERemoteServer.html unife/edgeRemote/exceptions/ unife/rmi/communication/RMIInputStream.html unife/rmi/communication/RMIOutputStream.html


cd ../../../..
# install into the local Maven repository
mvn install:install-file -Dfile=edge-interfaces-$1.jar \
    -Dsources=edge-interfaces-$1-sources.jar \
    -Djavadoc=edge-interfaces-$1-javadoc.jar \
    -DgroupId=unife \
    -DartifactId=edge-interfaces \
    -Dversion=$1 \
    -Dpackaging=jar

rm edge-interfaces-$1.jar edge-interfaces-$1-sources.jar edge-interfaces-$1-javadoc.jar
