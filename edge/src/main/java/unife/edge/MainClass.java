/**
 * This file is part of LEAP.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * LEAP is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * LEAP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package unife.edge;

import org.apache.log4j.PropertyConfigurator;

/**
 * Main class of EDGE application.
 *
 * @author Giuseppe Cota <giuseta@gmail.com>
 */
public class MainClass {

    public static void main(String[] args) {
        //PropertyConfigurator.configure("config/log4j.properties");
        String logPropertiesPath = "/config/log4j.properties";
        PropertyConfigurator.configure(MainClass.class.getResourceAsStream(logPropertiesPath));
        EDGEUI ui = EDGEStart.getUI(args);
        ui.run(args);
    }

}
