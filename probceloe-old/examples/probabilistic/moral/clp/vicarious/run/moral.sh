#!/bin/sh

PREFIX=/home/collab1/cota/test_leap/moral/clp/vicarious/
CONF_FILE=moral.conf
EDGE_LOG=log/edge.log
CELOE_LOG=log/CELOE.log

sh /home/collab1/cota/leap/bin/leap.sh "${PREFIX}$EDGE_LOG" "${PREFIX}$CELOE_LOG" "${PREFIX}$CONF_FILE"
