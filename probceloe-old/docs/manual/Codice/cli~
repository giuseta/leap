#!/bin/sh
# ----------------------------------------------------------------------------
#  Copyright 2001-2006 The Apache Software Foundation.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ----------------------------------------------------------------------------

#   Copyright (c) 2001-2002 The Apache Software Foundation.  All rights
#   reserved.

BASEDIR=`dirname $0`/..
BASEDIR=`(cd "$BASEDIR"; pwd)`
echo $BASEDIR
echo `pwd`


# OS specific support.  $var _must_ be set to either true or false.
cygwin=false;
darwin=false;
case "`uname`" in
  CYGWIN*) cygwin=true ;;
  Darwin*) darwin=true
           if [ -z "$JAVA_VERSION" ] ; then
             JAVA_VERSION="CurrentJDK"
           else
             echo "Using Java version: $JAVA_VERSION"
           fi
           if [ -z "$JAVA_HOME" ] ; then
             JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/${JAVA_VERSION}/Home
           fi
           ;;
esac

if [ -z "$JAVA_HOME" ] ; then
  if [ -r /etc/gentoo-release ] ; then
    JAVA_HOME=`java-config --jre-home`
  fi
fi

# For Cygwin, ensure paths are in UNIX format before anything is touched
if $cygwin ; then
  [ -n "$JAVA_HOME" ] && JAVA_HOME=`cygpath --unix "$JAVA_HOME"`
  [ -n "$CLASSPATH" ] && CLASSPATH=`cygpath --path --unix "$CLASSPATH"`
fi

# If a specific java binary isn't specified search for the standard 'java' binary
if [ -z "$JAVACMD" ] ; then
  if [ -n "$JAVA_HOME"  ] ; then
    if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
      # IBM's JDK on AIX uses strange locations for the executables
      JAVACMD="$JAVA_HOME/jre/sh/java"
    else
      JAVACMD="$JAVA_HOME/bin/java"
    fi
  else
    JAVACMD=`which java`
  fi
fi

if [ ! -x "$JAVACMD" ] ; then
  echo "Error: JAVA_HOME is not defined correctly."
  echo "  We cannot execute $JAVACMD"
  exit 1
fi

if [ -z "$REPO" ]
then
  REPO="$BASEDIR"/lib
fi

CLASSPATH=$CLASSPATH_PREFIX:/:"$REPO"/components-core-1.0-SNAPSHOT.jar:"$REPO"/sparql-0.2-20120229.222032-77.jar:"$REPO"/oval-1.31.jar:"$REPO"/aspectjrt-1.6.11.jar:"$REPO"/util-0.2-20120229.222032-78.jar:"$REPO"/collections-0.2-20120229.222032-80.jar:"$REPO"/collections-scala-0.2-20120229.222032-74.jar:"$REPO"/jena-core-2.7.1-incubating-20120330.212732-22.jar:"$REPO"/jena-iri-0.9.1-incubating-20120305.120450-12.jar:"$REPO"/icu4j-3.4.4.jar:"$REPO"/slf4j-log4j12-1.6.4.jar:"$REPO"/log4j-1.2.16.jar:"$REPO"/jena-arq-2.9.1-incubating-20120405.030724-84.jar:"$REPO"/commons-codec-1.5.jar:"$REPO"/httpclient-4.1.2.jar:"$REPO"/httpcore-4.1.3.jar:"$REPO"/pellet-2.3.0.jar:"$REPO"/owlapi-3.2.4.jar:"$REPO"/virtjdbc3-6.1.2.jar:"$REPO"/virt_jena-6.1.2.jar:"$REPO"/guava-r07.jar:"$REPO"/collections-generic-4.01.jar:"$REPO"/xstream-1.3.1.jar:"$REPO"/xpp3_min-1.1.4c.jar:"$REPO"/jamon-2.7.jar:"$REPO"/commons-validator-1.3.1.jar:"$REPO"/commons-beanutils-1.7.0.jar:"$REPO"/commons-digester-1.6.jar:"$REPO"/commons-collections-3.1.jar:"$REPO"/xml-apis-1.0.b2.jar:"$REPO"/velocity-1.5.jar:"$REPO"/commons-lang-2.3.jar:"$REPO"/oro-2.0.8.jar:"$REPO"/h2-1.2.143.jar:"$REPO"/commons-compress-1.2.jar:"$REPO"/slf4j-api-1.6.4.jar:"$REPO"/xercesImpl-2.6.0.jar:"$REPO"/json-20090211.jar:"$REPO"/jopt-simple-3.3.jar:"$REPO"/ini4j-0.5.2.jar:"$REPO"/xmlbeans-2.4.0.jar:"$REPO"/stax-api-1.0.1.jar:"$REPO"/factpp-owlapi-1.5.1.jar:"$REPO"/owllink-1.1.0.jar:"$REPO"/hermit-1.3.3.jar:"$REPO"/dig-xmlbeans-1.1.jar:"$REPO"/fuzzydl-1.0.jar:"$REPO"/colt-1.0.jar:"$REPO"/javacbc-1.0.jar:"$REPO"/fuzzyowl2fuzzydlparser-1.0.jar:"$REPO"/spring-beans-3.1.1.RELEASE.jar:"$REPO"/spring-core-3.1.1.RELEASE.jar:"$REPO"/spring-asm-3.1.1.RELEASE.jar:"$REPO"/jwnl-1.4.1.RC2.jar:"$REPO"/lucene-core-3.5.0.jar:"$REPO"/model-0.2-20120229.222032-78.jar:"$REPO"/json-simple-1.1.jar:"$REPO"/spring-context-3.1.1.RELEASE.jar:"$REPO"/spring-aop-3.1.1.RELEASE.jar:"$REPO"/aopalliance-1.0.jar:"$REPO"/spring-expression-3.1.1.RELEASE.jar:"$REPO"/spring-xmlbeans-3.0-1.0.jar:"$REPO"/jcl-over-slf4j-1.6.4.jar:"$REPO"/interfaces-1.0-SNAPSHOT.jar
EXTRA_JVM_ARGUMENTS="-Xms256m -Xmx1024m"

# For Cygwin, switch paths to Windows format before running java
if $cygwin; then
  [ -n "$CLASSPATH" ] && CLASSPATH=`cygpath --path --windows "$CLASSPATH"`
  [ -n "$JAVA_HOME" ] && JAVA_HOME=`cygpath --path --windows "$JAVA_HOME"`
  [ -n "$HOME" ] && HOME=`cygpath --path --windows "$HOME"`
  [ -n "$BASEDIR" ] && BASEDIR=`cygpath --path --windows "$BASEDIR"`
  [ -n "$REPO" ] && REPO=`cygpath --path --windows "$REPO"`
fi

exec "$JAVACMD" $JAVA_OPTS \
  $EXTRA_JVM_ARGUMENTS \
  -classpath "$CLASSPATH" \
  -Dapp.name="cli" \
  -Dapp.pid="$$" \
  -Dapp.repo="$REPO" \
  -Dbasedir="$BASEDIR" \
  org.dllearner.cli.CLI \
  "$@"
