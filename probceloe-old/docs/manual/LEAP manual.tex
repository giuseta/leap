\documentclass[a4paper, 12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{csquotes}
\usepackage[T1]{fontenc}
\usepackage[top=3cm, bottom=3cm, left=3cm, right=3cm]{geometry}
\usepackage{alltt}
\usepackage{makeidx}
% DL font
\newcommand{\dlf}[1]{% 
\ifx\@currenvir\@math%
\mathsf{#1}
\else%
\textsf{#1}}

\usepackage{color}
\definecolor{shadecolor}{gray}{0.9}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\usepackage{listings}
\lstset{ %
	%frame=single,
	literate={~} {$\sim$}{1},
	breaklines=true,
	backgroundcolor=\color{white},
	basicstyle=\ttfamily\scriptsize,
   	commentstyle=\color{mygreen}, 
   	keywordstyle=\color{blue},
	numberstyle=\small\color{mygray},
   	numbers=left,
   	numbersep=10pt,
   	rulecolor=\color{black},
	stringstyle=\color{mymauve},     
	tabsize=4,
	postbreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\color{red}\hookrightarrow\space}}  
   }
\usepackage{hyperref}
\hypersetup{
unicode=true,
pdftitle={A brief manual for LEAP},
pdfauthor={Giuseppe Cota},
pdfcreator={Giuseppe Cota},
linktoc=all,
hidelinks
}

\title{A brief manual for LEAP}
\author{Giuseppe Cota}
\date{}
\makeindex

% index without repeating
\newcommand{\ind}[1]{#1\index{#1}}
% very important without index
\newcommand{\vimp}[1]{\textbf{#1}}
% code style
\newcommand{\code}[1]{\texttt{#1}}
\begin{document}
\maketitle
This document is a short manual for LEAP. LEAP stands for "LEArning Probabilistic description logics".
\tableofcontents
\section{Introduction}
\ind{LEAP} is a client-server \ind{Java} \ind{RMI} application. 

The server side contains a class called EDGERemoteImpl, which implements the interface EDGERemote and performs the \ind{EDGE} algorithm. The client side, instead, runs a modified version of \ind{CELOE} called \ind{ProbCELOE} and a class called EDGE that does not perform the EDGE algorithm but invokes the remote methods of EDGERemote in order to compute the log-likelihood and the parameters.

LEAP was developed with the aim to produce a plugin for \ind{DL-Learner}. Indeed the \ind{RMI} client of LEAP is usable in \ind{DL-Learner}. To install LEAP, all you have to do is to include the compiled Jar file of the LEAP client in the classpath of DL-Learner when you run it, and pass to the DL-Learner interface\footnote{We tested and used DL-Learner only with the Command Line Interface.} the appropriate configuration file. Before you start \ind{DL-Learner} (and hence the client side of LEAP), you must first run the RMI server.

\section{How to use LEAP}
In this section is exposed how to install and run the LEAP system.
\subsection{Install}
To install LEAP you have to modify the file \texttt{<your\_path>/dllarner/bin/cli} and add the paths of \texttt{probceloe-xx.jar} and \texttt{edge-interfaces-xx.jar} into the \texttt{\$CLASSPATH} variable. Example:
\lstinputlisting[language=sh, linerange={76-81}, firstnumber=76]{Codice/cli}
In this example we copied and pasted \texttt{probceloe-0.2.jar} and \texttt{edge-interfaces-0.4.jar} into \texttt{<your\_path>/dllarner/lib} directory and then, as the reader can see in the last two lines, we added their path inside the \texttt{\$CLASSPATH} variable.
\subsection{Run}
In order to run LEAP, you need to do the following actions sequentially:
	\begin{enumerate}
		\item run the RMI server that implements the EDGE algorithm:					\begin{lstlisting}[breaklines, numbers=none]
$ java -d32 -cp edge/bundle2.jar:edge/lib/ \
 bundle.edgeserver.EDGERemoteFactoryImpl &
			\end{lstlisting}
		\item run DL-Learner:
			\begin{lstlisting}[numbers=none]
$ dllearner/bin/cli <configutation_file>
			\end{lstlisting}
	\end{enumerate}
\section{Available Options}
The CLI (Command Line Interface) of \ind{DL-Learner} takes as input a configuration file. This file contains all the information about the algorithms to use and their relative options.

There are several options for \ind{ProbCELOE} and \ind{EDGE} which can be set inside the configuration file that will be given as input to \ind{DL-Learner}. In this section we expose all the options of these two components, for more informations about the other components and configuration options of DL-Learner see http://sourceforge.net/p/dl-learner/code/HEAD/tree/trunk/interfaces/doc/configOptions.html.
\subsection{ProbCELOE Options}
We show here all the options for ProbCELOE:
\begin{description}
	\item[$\bullet$] all the options of CELOE (see DL-Learner documentation).
	\item[completeLearnedOntology] type: \texttt{String}. File path of the complete learned ontology. Default value: \texttt{completeLearnedOntology.owl}.
\end{description}
\subsubsection{Hidden Options (for Developers Only)}
In this section are listed the "hidden" options, which should not be used by a normal user. Indeed, the use of these options may not return correct values, so they must be set carefully. These options must be used by developers only, in order to test the results.
\begin{description}
	\item[saveAddedAxioms] type: \texttt{boolean}. If true the probabilistic added axioms are saved in an ontology file (see \ref{subsubsec:EDGE:hidden_options} \vimp{outputFile} option). Default value: \texttt{false}.
\end{description}
\subsection{EDGE Options}
The options of EDGE are:
\begin{description}
	\item[seed] type: \texttt{int}. Seed for random generation of probability values. Default value: \texttt{0}.
	\item[randomize] type: \texttt{boolean}. If true, the starting probabilities of the probabilistic axioms are randomized. Default value: \texttt{false}.
	\item[randomizeAll] type: \texttt{boolean}. If true, at the beginning of the algorithm, every axiom in the probabilistic ontology (including non probabilistic ones) will have a random probability. Default value: \texttt{false}.
	\item[differenceLL] type: \texttt{double}. Minimum stop difference between the log-likelihoods of two consecutive EM cycles. Default value: \texttt{0.000000000028}.
	\item[ratioLL] type: \texttt{double}. Minimum stop ratio between the log-likelihoods of two consecutive EM cycles. Default value: \texttt{0.000000000028}.
	\item[maxIterations] type: \texttt{long}. Maximum number of cycles. Default value: \texttt{2147000000}.
	\item[maxExplanations] type: \texttt{int}. The maximum number of explanations to find for each query. Default value: $\mathtt{Integer.MAX\_VALUE}$.
	\item[timeout] type: \texttt{int}. Max time allowed for inference (in seconds). Default value: \texttt{0 (infinite)}.
	\item[severAddress] type: \texttt{String}. It defines the IP address and the port of RMI server. It is string of the form <host>:<port> or only <host>; if only the host has been defined then the port will be 1099. Default value: \texttt{localhost:1099}
	\item[maxPositiveExamples] type: \texttt{int}. Maximum number of positive examples that EDGE must handle when a class learning problem is given. Default value: \texttt{0 (infinite)}.
	\item[maxNegativeExamples] type: \texttt{int}. Maximum number of negative examples that EDGE must handle when a class learning problem is given. Default value: \texttt{0 (infinite)}.
	\item[outputFileFormat] type: \texttt{String}\footnote{It is actually  not a String type, but in the configuration file you set this option by giving a string}. Format of the output file containing the learned ontology. Default value: \texttt{OWLXML}.
	\item[learnParametersFromScratch] type: \texttt{boolean}. If true EDGE learns from scratch the parameters of all the probabilistic axioms. Default value: \texttt{false}.
\end{description}
\subsubsection{Hidden Options (for Developers Only)}\label{subsubsec:EDGE:hidden_options}
In this section are listed the "hidden" options, which should not be used by a normal user. Indeed, the use of these options may not return correct values, so they must be set carefully. These options must be used by developers only, in order to test the results.
\begin{description}
	\item[outputFile] type: \texttt{String}. Output file containing only the learned probabilistic axioms. Default value: \texttt{<currentDir>/probs.owl}.
	\item[reduceOntology] type: \texttt{boolean}. Force the reduction of the number of individuals in the initial ontology. If true all the individuals which do not occur on the list of the positive or negative individuals are deleted from the input ontology.
	\item[useAlternativeEDGE] type: \texttt{boolean}. If false it uses the standard EDGE, if true it uses an alternative EDGE (an instance of EDGE algorithm is created every cycle. Default value: \texttt{false}.
\end{description}
\subsection{Other (for developers only)}
In this section are listed all the classes and options that should be used only be developers.
\subsubsection{CLI\_LEAP}
CLI\_LEAP is a new Command Line Interface for LEAP system. CLI\_LEAP is based on DL-Learner CLI, indeed, it has the same options. CLI\_LEAP was created essentially to allow a "cross validation" with LEAP. It is not perform a cross-validation, but a cross-training, in other words it splits the positive and negative individuals into $k$ folds, then CLI\_LEAP runs $k$ times LEAP and outputs $k$ learned knowledge bases. The developer should eventually execute the validation phase.

In order to perform the cross-training with LEAP you must write these lines in the configuration file:
\begin{lstlisting}[breaklines, numbers=none]
...
cli.type = "org.dllearner.cli.CLI_LEAP"
cli.performCrossValidation = true
cli.nrOfFolds = 5
...
\end{lstlisting}
If the number of folds is not given, the default value is used, that is 10.

With CLI\_LEAP is possible to perform a cross training using ProbCELOE and EDGE as learning algortithm and one of the following learning problem:
\begin{itemize}
	\item a sub class of \code{PosNegLP};
	\item \code{PosOnlyLP};
	\item \code{ClassLearningProblem}.
\end{itemize}
\section{A Complete Example}\label{sec:complete_example}
We illustrate here an example of how \ind{LEAP} system works. As said before LEAP has been implemented as a plugin for \ind{DL-Learner}, therefore we use the command line interface of DL-Learner to execute it. 

First of all let us consider the ontology \texttt{father.owl}. Due to problems with available space, we print here only some snippets. 
\lstinputlisting[language=XML, linerange={37-42}, firstnumber=37]{Codice/father.owl}

\lstinputlisting[language=XML, linerange={57-84}, firstnumber=57]{Codice/father.owl}

\lstinputlisting[language=XML, linerange={107-127}, firstnumber=107]{Codice/father.owl}

Let \texttt{father.conf} be the configuration file:
\lstinputlisting{Codice/father.conf}

Let us try to run the system on the terminal\footnote{We suppose we are on a Linux machine.}. First we need to start EDGE, which is our RMI server:
\begin{lstlisting}[breaklines, numbers=none]
$ java -d32 -cp edge/bundle2.jar:edge/lib/ \
     bundle.edgeserver.EDGERemoteFactoryImpl &
\end{lstlisting}
The output is:
\begin{lstlisting}[numbers=none]
 DEBUG [main] (EDGERemoteFactoryImpl.java:94) - Set default file policy /tmp/edgeServer5337565361874696090.policy
 DEBUG [main] (EDGERemoteFactoryImpl.java:105) - Set default codebase: file:/home/giuseppe/NetBeansProjects/bundle2/build/classes/
 INFO [main] (EDGERemoteFactoryImpl.java:128) - Launching RMIRegistry...
 INFO [main] (EDGERemoteFactoryImpl.java:131) - RMIRegistry launched!
 INFO [main] (EDGERemoteFactoryImpl.java:138) - EDGE remote factory registered. Listening on port: 1099
\end{lstlisting}
Now we can run DL-Learner and therefore ProbCELOE that uses a local EDGE class, which is our RMI client:
\begin{lstlisting}[numbers=none]
$ dllearner/bin/cli father.conf
\end{lstlisting}
The output of DL-Learner is:
\begin{lstlisting}
...
 INFO [main] (EDGE.java:289) - Initializing EDGE
 INFO [main] (EDGE.java:511) - Number of sources: 1
 INFO [main] (EDGE.java:512) - creating ontology through merging of the sources
 INFO [main] (EDGE.java:537) - Merging the ontologies...
 INFO [main] (EDGE.java:547) - Ontologies merged. Complete ontology created
 INFO [main] (ProbCELOE.java:478) - more accurate (75.00%) class expression found: male
 INFO [main] (ProbCELOE.java:478) - more accurate (100.00%) class expression found: (male and hasChild some person)
 INFO [main] (ProbCELOE.java:551) - CELOE algorithm terminated successfully (time: 1s 0ms, 1170 descriptions tested, 738 nodes in the search tree).

 INFO [main] (ProbCELOE.java:552) - number of retrievals: 33
retrieval reasoning time: 0ms ( 0ms per retrieval)
number of instance checks: 4830 (0 multiple)
instance check reasoning time: 208ms ( 0ms per instance check)
(complex) subsumption checks: 7 (0 multiple)
subsumption reasoning time: 39ms ( 5ms per subsumption check)
overall reasoning time: 248ms

 INFO [main] (ProbCELOE.java:560) - solutions:
1: (male and hasChild some person) 100.00%
2: (male and (female or hasChild some person)) 100.00%
3: (male and (not female) and hasChild some person) 100.00%
4: (male and ((not owl::Thing) or hasChild some person)) 100.00%
5: (male and ((not person) or hasChild some person)) 100.00%

 INFO [main] (ProbCELOE.java:606) - Initial Log-Likelihood: -41.44653167389282
 INFO [main] (ProbCELOE.java:635) - Adding axiom
 INFO [main] (ProbCELOE.java:641) - Axiom added.
 INFO [main] (ProbCELOE.java:642) - Running parameter Learner
 INFO [main] (ProbCELOE.java:645) - Current Log-Likelihood: 0.0
 INFO [main] (ProbCELOE.java:655) - Log-Likelihood enhanced. Updating ontologies...
 INFO [main] (ProbCELOE.java:635) - Adding axiom
 INFO [main] (ProbCELOE.java:641) - Axiom added.
 INFO [main] (ProbCELOE.java:642) - Running parameter Learner
 INFO [main] (ProbCELOE.java:645) - Current Log-Likelihood: 0.0
 INFO [main] (ProbCELOE.java:659) - Log-Likelihood worsened. Removing Last Axiom...
 INFO [main] (ProbCELOE.java:635) - Adding axiom
 INFO [main] (ProbCELOE.java:641) - Axiom added.
 INFO [main] (ProbCELOE.java:642) - Running parameter Learner
 INFO [main] (ProbCELOE.java:645) - Current Log-Likelihood: 0.0
 INFO [main] (ProbCELOE.java:659) - Log-Likelihood worsened. Removing Last Axiom...
 INFO [main] (ProbCELOE.java:635) - Adding axiom
 INFO [main] (ProbCELOE.java:641) - Axiom added.
 INFO [main] (ProbCELOE.java:642) - Running parameter Learner
 INFO [main] (ProbCELOE.java:645) - Current Log-Likelihood: 0.0
 INFO [main] (ProbCELOE.java:659) - Log-Likelihood worsened. Removing Last Axiom...
 INFO [main] (ProbCELOE.java:635) - Adding axiom
 INFO [main] (ProbCELOE.java:641) - Axiom added.
 INFO [main] (ProbCELOE.java:642) - Running parameter Learner
 INFO [main] (ProbCELOE.java:645) - Current Log-Likelihood: 0.0
 INFO [main] (ProbCELOE.java:659) - Log-Likelihood worsened. Removing Last Axiom...
 INFO [main] (ProbCELOE.java:570) - Structure Learning completed. Saving learned ontology
 INFO [main] (EDGE.java:1010) - Saving the complete learned ontology
 INFO [main] (EDGE.java:1036) - Saving positive Examples
 INFO [main] (EDGE.java:1065) - Saving negative Examples
 INFO [main] (ProbCELOE.java:589) - Main: 12756 ms
 INFO [main] (ProbCELOE.java:590) - CELOE: 1000 ms
 INFO [main] (ProbCELOE.java:591) - EDGE: 11436 ms
 INFO [main] (ProbCELOE.java:592) - 	Bundle: 11058 ms
 INFO [main] (ProbCELOE.java:593) - 	EM: 26 ms
 INFO [main] (ProbCELOE.java:595) - Other: 328 ms
 INFO [main] (ProbCELOE.java:596) - Program client: execution successfully terminated
\end{lstlisting}
Lines 2-6 of the output say that EDGE client and EDGE server have been initialized, the ontology, the positive and negative individuals and the target class $\dlf{Target}$ have been sent to the remote EDGE and the remote EDGE class received the set of individuals and for each individual creates an assertional axiom of the form $ a : \dlf{learnedClass}$. In lines 7-24 you see the output of CELOE and the 5 candidate axioms with their accuracy. Then lines 26-55 shows the workflow of the search in the space of theories. Lines 56-62 contain time statistics.

A careful reader has surely noted that, in the end, only the first axiom was added. This is because after the first axiom has been added the log-likelihood has reached the maximum possible value\footnote{Remember that the log-likelihood is a logarithm which has as argument a value between 0 and 1. So, if $a \in [0,1]$, the maximum value of $\log a$ is 0} 0. So from then on, it was impossible to improve the log-likelihood of the data. Therefore the program exited with only one axiom learned.

The output of the server side was not showed for brevity.

\printindex
\end{document}