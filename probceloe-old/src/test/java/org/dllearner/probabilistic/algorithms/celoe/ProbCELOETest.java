/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.dllearner.probabilistic.algorithms.celoe;

import org.dllearner.core.AbstractKnowledgeSource;
import org.dllearner.core.AbstractReasonerComponent;
import org.dllearner.core.ComponentInitException;
import org.dllearner.core.owl.NamedClass;
import org.dllearner.kb.OWLFile;
import org.dllearner.learningproblems.ClassLearningProblem;
import org.dllearner.probabilistic.core.AbstractLearningParameter;
import org.dllearner.probabilistic.edge.EDGE;
import org.dllearner.reasoning.FastInstanceChecker;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Spy;
import static org.mockito.Mockito.spy;
/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>
 */
public class ProbCELOETest {

    static AbstractKnowledgeSource ks;
    static AbstractReasonerComponent rc;
    static ClassLearningProblem lp;
    static AbstractLearningParameter lpr;

    public ProbCELOETest() {
    }

    @BeforeClass
    public static void setUpClass() throws ComponentInitException {
        ks = new OWLFile("examples/family/father_oe.owl");
        ks.init();

        rc = new FastInstanceChecker(ks);
        rc.init();

        lp = new ClassLearningProblem(rc);
        lp.setClassToDescribe(new NamedClass("http://example.com/father#father"));
        lp.setCheckConsistency(false);
        lp.init();

        lpr = new EDGE(lp, null);
        lpr.init();

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of supportedLearningProblems method, of class ProbCELOE.
     */
    @Test
    public void testSupportedLearningProblems() {
    }

    /**
     * Test of getName method, of class ProbCELOE.
     */
    @Test
    public void testGetName() {
    }

    /**
     * Test of init method, of class ProbCELOE.
     */
    @Test
    public void testInit() throws Exception {
    }

    /**
     * Test of getCurrentlyBestDescription method, of class ProbCELOE.
     */
    @Test
    public void testGetCurrentlyBestDescription() {
    }

    /**
     * Test of getCurrentlyBestDescriptions method, of class ProbCELOE.
     */
    @Test
    public void testGetCurrentlyBestDescriptions() {
    }

    /**
     * Test of getCurrentlyBestEvaluatedDescription method, of class ProbCELOE.
     */
    @Test
    public void testGetCurrentlyBestEvaluatedDescription() {
    }

    /**
     * Test of getCurrentlyBestEvaluatedDescriptions method, of class ProbCELOE.
     */
    @Test
    public void testGetCurrentlyBestEvaluatedDescriptions() {
    }

    /**
     * Test of getCurrentlyBestAccuracy method, of class ProbCELOE.
     */
    @Test
    public void testGetCurrentlyBestAccuracy() {
    }

    /**
     * Test of start method, of class ProbCELOE.
     */
    @Test
    public void testStart() {
    }

    /**
     * Test of isRunning method, of class ProbCELOE.
     */
    @Test
    public void testIsRunning() {
    }

    /**
     * Test of stop method, of class ProbCELOE.
     */
    @Test
    public void testStop() {
    }

    /**
     * Test of getSearchTreeRoot method, of class ProbCELOE.
     */
    @Test
    public void testGetSearchTreeRoot() {
    }

    /**
     * Test of getNodes method, of class ProbCELOE.
     */
    @Test
    public void testGetNodes() {
    }

    /**
     * Test of getMaximumHorizontalExpansion method, of class ProbCELOE.
     */
    @Test
    public void testGetMaximumHorizontalExpansion() {
    }

    /**
     * Test of getMinimumHorizontalExpansion method, of class ProbCELOE.
     */
    @Test
    public void testGetMinimumHorizontalExpansion() {
    }

    /**
     * Test of getClassExpressionTests method, of class ProbCELOE.
     */
    @Test
    public void testGetClassExpressionTests() {
    }

    /**
     * Test of getOperator method, of class ProbCELOE.
     */
    @Test
    public void testGetOperator() {
    }

    /**
     * Test of setOperator method, of class ProbCELOE.
     */
    @Test
    public void testSetOperator() {
    }

    /**
     * Test of getStartClass method, of class ProbCELOE.
     */
    @Test
    public void testGetStartClass() {
    }

    /**
     * Test of setStartClass method, of class ProbCELOE.
     */
    @Test
    public void testSetStartClass() {
    }

    /**
     * Test of getAllowedConcepts method, of class ProbCELOE.
     */
    @Test
    public void testGetAllowedConcepts() {
    }

    /**
     * Test of setAllowedConcepts method, of class ProbCELOE.
     */
    @Test
    public void testSetAllowedConcepts() {
    }

    /**
     * Test of getIgnoredConcepts method, of class ProbCELOE.
     */
    @Test
    public void testGetIgnoredConcepts() {
    }

    /**
     * Test of setIgnoredConcepts method, of class ProbCELOE.
     */
    @Test
    public void testSetIgnoredConcepts() {
    }

    /**
     * Test of isWriteSearchTree method, of class ProbCELOE.
     */
    @Test
    public void testIsWriteSearchTree() {
    }

    /**
     * Test of setWriteSearchTree method, of class ProbCELOE.
     */
    @Test
    public void testSetWriteSearchTree() {
    }

    /**
     * Test of getSearchTreeFile method, of class ProbCELOE.
     */
    @Test
    public void testGetSearchTreeFile() {
    }

    /**
     * Test of setSearchTreeFile method, of class ProbCELOE.
     */
    @Test
    public void testSetSearchTreeFile() {
    }

    /**
     * Test of getMaxNrOfResults method, of class ProbCELOE.
     */
    @Test
    public void testGetMaxNrOfResults() {
    }

    /**
     * Test of setMaxNrOfResults method, of class ProbCELOE.
     */
    @Test
    public void testSetMaxNrOfResults() {
    }

    /**
     * Test of getNoisePercentage method, of class ProbCELOE.
     */
    @Test
    public void testGetNoisePercentage() {
    }

    /**
     * Test of setNoisePercentage method, of class ProbCELOE.
     */
    @Test
    public void testSetNoisePercentage() {
    }

    /**
     * Test of isFilterDescriptionsFollowingFromKB method, of class ProbCELOE.
     */
    @Test
    public void testIsFilterDescriptionsFollowingFromKB() {
    }

    /**
     * Test of setFilterDescriptionsFollowingFromKB method, of class ProbCELOE.
     */
    @Test
    public void testSetFilterDescriptionsFollowingFromKB() {
    }

    /**
     * Test of isReplaceSearchTree method, of class ProbCELOE.
     */
    @Test
    public void testIsReplaceSearchTree() {
    }

    /**
     * Test of setReplaceSearchTree method, of class ProbCELOE.
     */
    @Test
    public void testSetReplaceSearchTree() {
    }

    /**
     * Test of getMaxClassDescriptionTests method, of class ProbCELOE.
     */
    @Test
    public void testGetMaxClassDescriptionTests() {
    }

    /**
     * Test of setMaxClassDescriptionTests method, of class ProbCELOE.
     */
    @Test
    public void testSetMaxClassDescriptionTests() {
    }

    /**
     * Test of getMaxExecutionTimeInSeconds method, of class ProbCELOE.
     */
    @Test
    public void testGetMaxExecutionTimeInSeconds() {
    }

    /**
     * Test of setMaxExecutionTimeInSeconds method, of class ProbCELOE.
     */
    @Test
    public void testSetMaxExecutionTimeInSeconds() {
    }

    /**
     * Test of isTerminateOnNoiseReached method, of class ProbCELOE.
     */
    @Test
    public void testIsTerminateOnNoiseReached() {
    }

    /**
     * Test of setTerminateOnNoiseReached method, of class ProbCELOE.
     */
    @Test
    public void testSetTerminateOnNoiseReached() {
    }

    /**
     * Test of isReuseExistingDescription method, of class ProbCELOE.
     */
    @Test
    public void testIsReuseExistingDescription() {
    }

    /**
     * Test of setReuseExistingDescription method, of class ProbCELOE.
     */
    @Test
    public void testSetReuseExistingDescription() {
    }

    /**
     * Test of isUseMinimizer method, of class ProbCELOE.
     */
    @Test
    public void testIsUseMinimizer() {
    }

    /**
     * Test of setUseMinimizer method, of class ProbCELOE.
     */
    @Test
    public void testSetUseMinimizer() {
    }

    /**
     * Test of getHeuristic method, of class ProbCELOE.
     */
    @Test
    public void testGetHeuristic() {
    }

    /**
     * Test of setHeuristic method, of class ProbCELOE.
     */
    @Test
    public void testSetHeuristic() {
    }

    /**
     * Test of getMaxClassExpressionTestsWithoutImprovement method, of class
     * ProbCELOE.
     */
    @Test
    public void testGetMaxClassExpressionTestsWithoutImprovement() {
    }

    /**
     * Test of setMaxClassExpressionTestsWithoutImprovement method, of class
     * ProbCELOE.
     */
    @Test
    public void testSetMaxClassExpressionTestsWithoutImprovement() {
    }

    /**
     * Test of getMaxExecutionTimeInSecondsAfterImprovement method, of class
     * ProbCELOE.
     */
    @Test
    public void testGetMaxExecutionTimeInSecondsAfterImprovement() {
    }

    /**
     * Test of setMaxExecutionTimeInSecondsAfterImprovement method, of class
     * ProbCELOE.
     */
    @Test
    public void testSetMaxExecutionTimeInSecondsAfterImprovement() {
    }

    /**
     * Test of isSingleSuggestionMode method, of class ProbCELOE.
     */
    @Test
    public void testIsSingleSuggestionMode() {
    }

    /**
     * Test of setSingleSuggestionMode method, of class ProbCELOE.
     */
    @Test
    public void testSetSingleSuggestionMode() {
    }

    /**
     * Test of getMaxClassExpressionTests method, of class ProbCELOE.
     */
    @Test
    public void testGetMaxClassExpressionTests() {
    }

    /**
     * Test of setMaxClassExpressionTests method, of class ProbCELOE.
     */
    @Test
    public void testSetMaxClassExpressionTests() {
    }

    /**
     * Test of getMaxClassExpressionTestsAfterImprovement method, of class
     * ProbCELOE.
     */
    @Test
    public void testGetMaxClassExpressionTestsAfterImprovement() {
    }

    /**
     * Test of setMaxClassExpressionTestsAfterImprovement method, of class
     * ProbCELOE.
     */
    @Test
    public void testSetMaxClassExpressionTestsAfterImprovement() {
    }

    /**
     * Test of getMaxDepth method, of class ProbCELOE.
     */
    @Test
    public void testGetMaxDepth() {
    }

    /**
     * Test of setMaxDepth method, of class ProbCELOE.
     */
    @Test
    public void testSetMaxDepth() {
    }

    /**
     * Test of isStopOnFirstDefinition method, of class ProbCELOE.
     */
    @Test
    public void testIsStopOnFirstDefinition() {
    }

    /**
     * Test of setStopOnFirstDefinition method, of class ProbCELOE.
     */
    @Test
    public void testSetStopOnFirstDefinition() {
    }

    /**
     * Test of getTotalRuntimeNs method, of class ProbCELOE.
     */
    @Test
    public void testGetTotalRuntimeNs() {
    }

    /**
     * Test of isExpandAccuracy100Nodes method, of class ProbCELOE.
     */
    @Test
    public void testIsExpandAccuracy100Nodes() {
    }

    /**
     * Test of setExpandAccuracy100Nodes method, of class ProbCELOE.
     */
    @Test
    public void testSetExpandAccuracy100Nodes() {
    }

    /**
     * Test of main method, of class ProbCELOE.
     */
    @Test
    public void testMain() throws Exception {
    }

    /**
     * Test of getDifferenceLL method, of class ProbCELOE.
     */
    @Test
    public void testGetDifferenceLL() {
    }

    /**
     * Test of setDifferenceLL method, of class ProbCELOE.
     */
    @Test
    public void testSetDifferenceLL() {
    }

    /**
     * Test of getRatioLL method, of class ProbCELOE.
     */
    @Test
    public void testGetRatioLL() {
    }

    /**
     * Test of setRatioLL method, of class ProbCELOE.
     */
    @Test
    public void testSetRatioLL() {
    }

    /**
     * Test of getLearningParameter method, of class ProbCELOE.
     */
    @Test
    public void testGetLearningParameter() {
    }

    /**
     * Test of setLearningParameter method, of class ProbCELOE.
     */
    @Test
    public void testSetLearningParameter() {
    }

    @Test
    public void testAdd2axiom() {
        ProbCELOE alg = new ProbCELOE(lpr, lp, rc);
        ProbCELOE spy = spy(alg);
        

    }

}
