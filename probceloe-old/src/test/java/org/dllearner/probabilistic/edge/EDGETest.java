/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.dllearner.probabilistic.edge;

import unife.edgeRemote.EDGERemote;
import unife.edgeRemote.exceptions.InconsistencyException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;
import junit.framework.TestCase;
import org.dllearner.core.AbstractKnowledgeSource;
import org.dllearner.core.AbstractReasonerComponent;
import org.dllearner.core.ComponentInitException;
import org.dllearner.core.owl.Axiom;
import org.dllearner.core.owl.Description;
import org.dllearner.core.owl.Individual;
import org.dllearner.core.owl.Intersection;
import org.dllearner.core.owl.NamedClass;
import org.dllearner.core.owl.ObjectProperty;
import org.dllearner.core.owl.ObjectSomeRestriction;
import org.dllearner.core.owl.Thing;
import org.dllearner.core.owl.Union;
import org.dllearner.kb.OWLFile;
import org.dllearner.learningproblems.ClassLearningProblem;
import org.dllearner.learningproblems.PosNegLPStandard;
import org.dllearner.learningproblems.PosOnlyLP;
import org.dllearner.probabilistic.core.ParameterLearnerException;
import org.dllearner.reasoning.FastInstanceChecker;
import org.dllearner.utilities.ReflectionHelper;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>
 */
@RunWith(Parameterized.class)
public class EDGETest extends TestCase {

    int intValue;
    boolean booleanValue;
    double doubleValue;
    static ClassLearningProblem celp;
    static PosNegLPStandard pnlp;
    static PosOnlyLP polp;
    static SortedSet<Individual> positiveIndividuals;
    static SortedSet<Individual> negativeIndividuals;

//    public EDGETest(String testName) {
//        super(testName);
//    }
    public EDGETest(double doubleValue, int intValue, boolean booleanValue) {
        this.doubleValue = doubleValue;
        this.intValue = intValue;
        this.booleanValue = booleanValue;
    }

    @BeforeClass
    public static void setUpClass() throws ComponentInitException {

        positiveIndividuals = new TreeSet<Individual>();
        positiveIndividuals.add(new Individual("http://example.com/father#stefan"));
        positiveIndividuals.add(new Individual("http://example.com/father#markus"));
        positiveIndividuals.add(new Individual("http://example.com/father#martin"));

        negativeIndividuals = new TreeSet<Individual>();
        negativeIndividuals.add(new Individual("http://example.com/father#heinz"));
        negativeIndividuals.add(new Individual("http://example.com/father#anna"));
        negativeIndividuals.add(new Individual("http://example.com/father#michelle"));

        AbstractKnowledgeSource ks = new OWLFile("examples/family/father_oe.owl");
        ks.init();

        AbstractReasonerComponent rc = new FastInstanceChecker(ks);
        rc.init();

        pnlp = new PosNegLPStandard(rc, positiveIndividuals, negativeIndividuals);

        polp = new PosOnlyLP(rc, positiveIndividuals);

        celp = new ClassLearningProblem(rc);
        celp.setClassToDescribe(new NamedClass("http://example.com/father#father"));
        celp.setCheckConsistency(false);
        celp.init();
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{0.2, 1, true},
        {-5.0, 10, true},
        {121.0, 20, false}};
        return Arrays.asList(data);
    }

    /**
     * Test of getParameter method, of class EDGE.
     */
    @Test
    public void testGetParameter() throws ParameterLearnerException {
        System.out.println("getParameter");
        Axiom ax = null;
        EDGE instance = new EDGE();
        double expResult = 0.0;
        double result = instance.getParameter(ax);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getLL method, of class EDGE.
     */
    @Test
    public void testGetLL() {
        System.out.println("getLL");
        EDGE instance = new EDGE();
        double expResult = 0.0;
        double result = instance.getLL();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class EDGE.
     */
    @Test
    public void testInit() throws Exception {
        System.out.println("init");
        //String pathGrantAll = createPolicyFile();
        //@Spy add later
        EDGE instance = new EDGE(celp, null);
        // add later
        //doReturn(pathGrantAll).when(instance).createPolicyFile();
        instance.init();
        EDGERemote eDGERemote = ReflectionHelper.getPrivateField(instance, "eDGERemote");
        assertEquals("EDGERemoteImpl", eDGERemote.getName());
    }

    /**
     * Test of getName method, of class EDGE.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String expResult = "EDGE";
        String result = EDGE.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of test method, of class EDGE.
     */
    @Test
    public void testTest() {
        System.out.println("test");
        EDGE instance = new EDGE();
        instance.test();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of run method, of class EDGE.
     */
    @Test
    public void testRun() throws ComponentInitException, ParameterLearnerException, InconsistencyException, IOException {
        System.out.println("run");
        //EDGE instance = new EDGE(celp, null);
        EDGE instance = new EDGE(pnlp, null);
        instance.setMaxIterations(100000);
        instance.init();
        Description description = new Union(new NamedClass("http://example.com/father#father"), new Thing());
        instance.addAxiom(description, 0.3);
        instance.run();
        System.out.println("Loglikelihood: " + instance.getLL());
        instance.updateOntology();
        ObjectProperty partial1 = new ObjectProperty("http://example.com/father#hasChild");
        Description partial2 = new ObjectSomeRestriction(partial1, new NamedClass("http://example.com/father#person"));
        Description ce = new Intersection(new NamedClass("http://example.com/father#male"), partial2);
        instance.addAxiom(ce, 0.2);
        instance.run();
        System.out.println("Loglikelihood: " + instance.getLL());
        instance.removeLastAxiom();
        //instance.run();
        System.out.println("Loglikelihood: " + instance.getLL());
        instance.saveLearnedOntology(new File("learnedOntology.owl"));
    }

    /**
     * Test of setSeed and getSeed methods, of class EDGE.
     */
    @Test
    public void testSeed() {
        System.out.println("getSeed");
        EDGE instance = new EDGE();
        int expResult = 2;
        instance.setSeed(expResult);
        int result = instance.getSeed();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRandomize and isRandomize method, of class EDGE.
     */
    @Test
    public void testRandomize() {
        System.out.println("isRandomize");
        EDGE instance = new EDGE();
        boolean expResult = true;
        instance.setRandomize(expResult);
        boolean result = instance.isRandomize();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRandomizeAll and isRandomizeAll methods, of class EDGE.
     */
    @Test
    public void testRandomizeAll() {
        System.out.println("isRandomizeAll");
        EDGE instance = new EDGE();
        boolean expResult = true;
        instance.setRandomizeAll(expResult);
        boolean result = instance.isRandomizeAll();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDifferenceLL and getDifferenceLL methods, of class EDGE.
     */
    @Test
    public void testDifferenceLL() {
        System.out.println("getDifferenceLL");
        EDGE instance = new EDGE();
        instance.setDifferenceLL(doubleValue);
        double result = instance.getDifferenceLL();
        if (doubleValue <= 0) {
            assertNotSame(doubleValue, result);
        } else {
            assertEquals(doubleValue, result);
        }
    }

    /**
     * Test of setRatioLL and getRatioLL methods, of class EDGE.
     */
    @Test
    public void testRatioLL() {
        System.out.println("getRatioLL");
        EDGE instance = new EDGE();
        instance.setRatioLL(doubleValue);
        double result = instance.getRatioLL();
        if (doubleValue <= 0 || doubleValue > 1) {
            assertNotSame(doubleValue, result);
        } else {
            assertEquals(doubleValue, result);
        }
    }

    /**
     * Test of setMaxIterations and getMaxIterations methods, of class EDGE.
     */
    @Test
    public void testMaxIterations() {
        System.out.println("getMaxIterations");
        EDGE instance = new EDGE();
        long expResult = intValue;
        instance.setMaxIterations(expResult);
        long result = instance.getMaxIterations();
        if (expResult <= 0) {
            assertNotSame(expResult, result);
        } else {
            assertEquals(expResult, result);
        }
    }

    /**
     * Test of setMaxExplanations and getMaxExplanations methods, of class EDGE.
     */
    @Test
    public void testMaxExplanations() {
        System.out.println("getMaxExplanations");
        EDGE instance = new EDGE();
        instance.setMaxExplanations(intValue);
        int result = instance.getMaxExplanations();
        if (intValue <= 0) {
            assertNotSame(intValue, result);
        } else {
            assertEquals(intValue, result);
        }
    }

    /**
     * Test of setTimeout and getTimeout methods, of class EDGE.
     */
    @Test
    public void testTimeout() {
        System.out.println("getTimeout");
        EDGE instance = new EDGE();
        instance.setTimeout(intValue);
        int result = instance.getTimeout();
        if (intValue <= 0) {
            assertNotSame(intValue, result);
        } else {
            assertEquals(intValue, result);
        }
    }

    /**
     * Test of setShowAll and isShowAll methods, of class EDGE.
     */
    @Test
    public void testShowAll() {
        System.out.println("isShowAll");
        EDGE instance = new EDGE();
        instance.setShowAll(booleanValue);
        boolean result = instance.isShowAll();
        assertEquals(booleanValue, result);
    }

    /**
     * Test of setOutputFileFormat and getOutputFileFormat methods, of class
     * EDGE.
     */
    @Test
    public void testOutputFileFormat() {
        System.out.println("getOutputFileFormat");
        EDGE instance = new EDGE();
        EDGERemote.PossibleOutputFileFormat expResult = EDGERemote.PossibleOutputFileFormat.RDFXML;
        instance.setOutputFileFormat(expResult);
        EDGERemote.PossibleOutputFileFormat result = instance.getOutputFileFormat();
        assertEquals(expResult, result);
    }

    /**
     * Test of getServerAddress method, of class EDGE.
     */
    @Test
    public void testGetServerAddress() {
        System.out.println("getServerAddress");
        EDGE instance = new EDGE();
        String expResult = "";
        String result = instance.getServerAddress();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setServerAddress method, of class EDGE.
     */
    @Test
    public void testSetServerAddress() {
        System.out.println("setServerAddress");
        String serverAddress = "";
        EDGE instance = new EDGE();
        instance.setServerAddress(serverAddress);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of exit method, of class EDGE.
     */
    @Test
    public void testExit() throws RemoteException {
        System.out.println("exit");
        EDGE instance = new EDGE();
        instance.exit();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addAxiom method, of class EDGE.
     */
    @Test
    public void testAddAxiom() throws Exception {
        System.out.println("addAxiom");
        Description classExpression = null;
        double epistemicProb = 0.0;
        EDGE instance = new EDGE();
        instance.addAxiom(classExpression, epistemicProb);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removeLastAxiom method, of class EDGE.
     */
    @Test
    public void testRemoveLastAxiom() throws Exception {
        System.out.println("removeLastAxiom");
        EDGE instance = new EDGE();
        instance.removeLastAxiom();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updateOntology method, of class EDGE.
     */
    @Test
    public void testUpdateOntology() throws Exception {
        System.out.println("updateOntology");
        EDGE instance = new EDGE();
        instance.updateOntology();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of saveLearnedOntology method, of class EDGE.
     */
    @Test
    public void testSaveLearnedOntology() throws Exception {
        System.out.println("saveLearnedOntology");
        EDGE instance = new EDGE();
        instance.saveLearnedOntology(new File("learnedOntology.owl"));
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of saveCreatedOntology method, of class EDGE.
     */
    @Test
    public void testSaveCreatedOntology() throws Exception {
        System.out.println("saveCreatedOntology");
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(IRI.create(new File("examples/family/father_oe.owl")));
        EDGE instance = new EDGE();
        String result = instance.saveCreatedOntology(ontology);
        assertTrue(new File(result).length() > 0);
    }

    private String createPolicyFile() {
        String str = " grant {\n"
                + "\tpermission java.security.AllPermission; \n"
                + "};";
        File policyFile = null;

        try {
            policyFile = File.createTempFile("edgeClient", ".policy");
            policyFile.deleteOnExit();
        } catch (IOException ex) {
            //logger.fatal("Fatal error creating the temporary policy file", ex);
            System.exit(-1);
        }

        try {
            PrintWriter writer = new PrintWriter(new FileWriter(policyFile));
            writer.println(str);
            writer.close();
        } catch (IOException ex) {
            System.out.println("Error writing policy file. See log for more details");
            //logger.fatal(ex.getMessage());
            System.exit(-1);
        }

        return policyFile.getAbsolutePath();
    }
}
