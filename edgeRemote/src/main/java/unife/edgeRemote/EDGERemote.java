/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edgeRemote;

//import bundle.edgeserver.RMISerializableOutputStream;
import java.io.File;
import unife.edgeRemote.exceptions.InconsistencyException;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;
import org.dllearner.core.owl.Description;
import org.dllearner.core.owl.NamedClass;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import unife.edge.EDGEStat;

/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese <riccardo.zese@unife.it>
 */
public interface EDGERemote extends Remote {

    /**
     * All the possible file format for the output (learned) ontology file.
     */
    public enum PossibleOutputFileFormat {

        OWLXML, OWLFUNCTIONAL, RDFXML
    }
    
    /**
     * All the possible type of EDGE MPI Scheduling
     */
    public enum EDGEMPISchedulingType {
        NONE, // no parallelization, no MPI
        SINGLE, // Single-Step
        DYNAMIC // Dynamic
    }

    /**
     * This method adds the axiom to the current ontology used by EDGE. The
     * axiom is in Manchester syntax.
     *
     * @param axiom Axiom to be added
     * @throws RemoteException
     * @throws bundle.edgeserver.interfaces.exception.InconsistencyException
     */
    //public void addAxiom(String axiom) throws RemoteException, InconsistencyException;

    /**
     * Adds SubClassOf axiom into the current ontology. It adds into the
     * ontology the axiom "classExpression subClassOf dummyClass". The
     * dummyClass will be substituted by the atomic concept that we want to
     * describe
     *
     * @param classExpression candidate class expression of the atomic concept
     * @param epistemicProb epistemic probability of the axiom
     * @throws RemoteException
     * @throws unife.edgeRemote.exceptions.InconsistencyException
     */
    public void addSubClassOfAxiom(Description classExpression, double epistemicProb) throws RemoteException, InconsistencyException;

    /**
     * Adds SubClassOf axiom into the current ontology. It adds into the
     * ontology the axiom "classExpression subClassOf targetClass".
     *
     * @param classExpression candidate class expression of the atomic concept
     * @param targetClass concept to describe
     * @param epistemicProb epistemic probability of the axiom
     * @throws RemoteException
     * @throws unife.edgeRemote.exceptions.InconsistencyException
     */
    public void addSubClassOfAxiom(Description classExpression, Description targetClass, double epistemicProb) throws RemoteException, InconsistencyException;

    /**
     * Updates the current learned ontology. This is a mandatory method to call
     * when the Log-Likelihood improved. This is a method that you must call if
     * you want to update the probability values of the learned ontology, after
     * it was proved that the Log-Likelihood improved. Do not call it after a
     * {@link #removeLastAxiom()} or you will get wrong values.
     *
     * @throws RemoteException
     */
    public void updateOntology() throws RemoteException;

    /**
     * Sets the initial ontology used by EDGE. The input ontology is a string.
     *
     * @param ontologyString all the ontology in a string
     * @throws RemoteException
     */
    public void setOntology(String ontologyString) throws RemoteException;
    
    /**
     * Sets the initial ontology used by EDGE.
     *
     * @param ontologyFile URL of the file containing the ontology
     * @throws RemoteException
     */
    //public void setOntology(URL ontologyFile) throws RemoteException;

    /**
     * Sets the initial ontology used by EDGE. This method works only if the RMI
     * client is on the same machine of the server.
     *
     * @param ontologyPath absolute or relative path of the file containing the
     * ontology
     * @throws RemoteException
     */
    //public void setOntology(String ontologyPath) throws RemoteException;

    /**
     * Sets the initial ontology used by EDGE. There could be problems if the
     * server and the client versions of owlapi are different
     *
     * @param ontology
     * @throws RemoteException
     */
    public void setOntology(OWLOntology ontology) throws RemoteException;

    /**
     * Sets the initial ontology used by EDGE. This method reads the remote
     * output stream and load the ontology.
     *
     * @param f
     * @throws IOException
     * @throws RemoteException
     */
    public void setOntology(File f) throws IOException, RemoteException;
    
    /**
     * Sets the positive Examples for EDGE
     *
     * @param positiveIndividuals set of individuals belonging to the class we
     * want to describe
     * @throws RemoteException
     */
    public void setPositiveExamples(Set<String> positiveIndividuals) throws RemoteException;

    /**
     * Sets the positive Examples for EDGE. NOT SUPPORTED YET!!
     *
     * @param positiveIndividuals
     * @param classToDescribe
     * @throws RemoteException
     */
    public void setPositiveExamples(Set<String> positiveIndividuals, NamedClass classToDescribe) throws RemoteException;

    /**
     * Sets the negative Examples for EDGE
     *
     * @param negativeIndividuals set of individuals NOT belonging to the class
     * we want to describe
     * @throws RemoteException
     */
    public void setNegativeExamples(Set<String> negativeIndividuals) throws RemoteException;

    /**
     * Sets the negative Examples for EDGE. NOT SUPPORTED YET!!
     *
     * @param negativeIndividuals
     * @param superClass
     * @throws RemoteException
     */
    public void setNegativeExamples(Set<String> negativeIndividuals, Description superClass) throws RemoteException;

    // It is not created a method to set a probabilityFile, see addAxiom
    /**
     * This method sets the starting seed. When we want to randomize the initial
     * probabilities of the axioms, this seed is given to the random generator.
     *
     * @param seed
     * @throws java.rmi.RemoteException
     */
    public void setSeed(int seed) throws RemoteException;

    /**
     * Setter for randomize variable. If set to true the starting probabilities
     * of the target axioms are random
     *
     * @param randomize
     * @throws java.rmi.RemoteException
     */
    public void setRandomize(boolean randomize) throws RemoteException;

    /**
     * Setter for randomizeAll variable. If set to true ALL the starting
     * probabilities of the axiom in the starting ontology are random
     *
     * @param randomizeAll
     * @throws java.rmi.RemoteException
     */
    public void setRandomizeAll(boolean randomizeAll) throws RemoteException;

    /**
     * Setter for difference of Log-Likelihoods. This difference between the
     * previous and the current Log-Likelihood calculated after the
     * Expectation-Maximization cycle is a termination criteria of EDGE
     * algorithm
     *
     * @param diff
     * @throws RemoteException
     */
    public void setDifferenceLL(String diff) throws RemoteException;

    /**
     * Setter for ratio of Log-Likelihoods. This ratio between the previous and
     * the current Log-Likelihood calculated after the Expectation-Maximization
     * cycle is a termination criteria of EDGE algorithm
     *
     * @param ratio
     * @throws RemoteException
     */
    public void setRatioLL(String ratio) throws RemoteException;

    /**
     * Setter for the max number of iterations that EDGE can perform
     *
     * @param iter max number of iterations
     * @throws RemoteException
     */
    public void setMaxIterations(long iter) throws RemoteException;

    /**
     * Setter for the max number of explanations that BUNDLE can perform
     *
     * @param numEx max number of explanations
     * @throws RemoteException
     */
    public void setMaxExplanations(int numEx) throws RemoteException;

    /**
     * Setter for EDGE Timeout
     *
     * @param timeout Timeout in seconds
     * @throws RemoteException
     */
    public void setTimeout(int timeout) throws RemoteException;

    /**
     * Setter for the showAll option
     *
     * @param showAll if true shows verbose information
     * @throws RemoteException
     */
    public void setShowAll(boolean showAll) throws RemoteException;
    
    /**
     * Setter for the merge option
     * 
     * 
     * @param merge if true the returned learned ontology is the result 
     * of the merge of the background ontology and the probabilistic ontology. 
     * Otherwise only the probabilistic ontology is returned, where the probabilistic
     * ontology is the ontology containing probabilistic axioms only.
     * @throws java.rmi.RemoteException
     */
    public void setMerge(boolean merge) throws RemoteException;

    /**
     * Sets the format for the output (learned) ontology. See
     * {@link PossibleOutputFileFormat} for all the possible file format
     *
     * @param format A format file between
     * @throws RemoteException
     */
    public void setOutputFileFormat(PossibleOutputFileFormat format) throws RemoteException;

    /**
     * Performs the EDGE algorithm
     *
     * @return an object containing information as the computed Log-Likelihood
     * and the probability of the added axiom into the ontology
     * @throws RemoteException
     */
    public EDGEStat computeLearning() throws RemoteException;

    /**
     * Initializes the EDGE Class.
     *
     * @throws RemoteException
     */
    //public void init() throws RemoteException;
    
    /**
     * Removes the last added axiom from the current ontology.
     *
     * @throws java.rmi.RemoteException
     */
    public void removeLastAxiom() throws RemoteException;

    /**
     * Sets the current concept that we want to describe.
     *
     * @param concept
     * @throws RemoteException
     */
    public void setConceptToDescribe(Description concept) throws RemoteException;

    /**
     * Gets the complete learned ontology containing both non-probabilistic
     * axioms and probabilistic axioms
     *
     * @return
     * @throws RemoteException
     */
    public OWLOntology getLearnedOntology() throws RemoteException;

    /**
     * Gets the input stream necessary for reading the complete learned
     * ontology.
     *
     * @return input stream of the complete learned ontology
     * @throws RemoteException
     * @throws IOException
     */
    public InputStream getLearnedOntologyInputStream() throws RemoteException, IOException;

    /**
     * Returns the name of the class.
     *
     * @return the name of the instance
     * @throws java.rmi.RemoteException
     */
    public String getName() throws RemoteException;
    
    /**
     * Gets an ontology containing the positive examples (assertional axioms).
     * ATTENTION: use this method only if you use the same owlapi version of
     * EDGE
     *
     * @return the ontology containing the positive examples
     * @throws java.rmi.RemoteException
     */
    public OWLOntology getPositiveExamplesOntology() throws RemoteException;

    /**
     * Gets an ontology containing the negative examples. ATTENTION: use this
     * method only if you use the same owlapi version of EDGE
     *
     * @return the ontology containing the negative examples
     * @throws java.rmi.RemoteException
     */
    public OWLOntology getNegativeExamplesOntology() throws RemoteException;

    /**
     * Gets the set of positive examples. ATTENTION: use this method only if you
     * use the same owlapi version of EDGE
     *
     * @return the set of positive examples
     * @throws RemoteException
     */
    public List<OWLAxiom> getPositiveExamples() throws RemoteException;

    /**
     * Gets the input stream necessary for reading the ontology containing the
     * positive examples.
     *
     * @return input stream of the ontology containing the negative examples
     * @throws RemoteException
     */
    public InputStream getPositiveExamplesInputStream() throws RemoteException, IOException;

    /**
     * Gets the set of negative examples. ATTENTION: use this method only if you
     * use the same owlapi version of EDGE
     *
     * @return the set of negative examples
     * @throws RemoteException
     */
    public List<OWLAxiom> getNegativeExamples() throws RemoteException;

    /**
     * Gets the input stream necessary for reading the ontology containing the
     * negative examples.
     *
     * @return input stream of the ontology containing the negative examples
     * @throws RemoteException
     */
    public InputStream getNegativeExamplesInputStream() throws RemoteException, IOException;

    /**
     * Setter for keepParameters variable. If set to true EDGE
     * keeps the old parameter values of all the probabilistic axioms and it does not relearn them.
     *
     * @param keepParameters
     * @throws RemoteException
     */
    //public void setKeepParameters(Boolean keepParameters) throws RemoteException;
}
