/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edgeRemote;

import unife.edgeRemote.EDGERemote;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Set;

/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese <riccardo.zese@unife.it>
 */
public interface EDGERemoteServer extends Remote {

    /**
     * Creates a stub of EDGERemoteImpl and send it to the client. It uses a
     * basic constructor of EdgeRemoteImpl.
     * DEPRECATED!!
     * @param ontologyFilename
     * @param positiveIndividuals
     * @param negativeIndividuals
     * @return
     * @throws RemoteException
     */
    //EDGERemote createEDGE(String ontologyFilename, Set<String> positiveIndividuals, Set<String> negativeIndividuals) throws RemoteException;

    /**
     * Creates a stub of EDGERemoteImpl and send it to the client. It uses the
     * empty constructor of EDGERemoteImpl, all the parameters must be set,
     * including the essential ones.
     *
     * @return a stub of EDGERemoteImpl
     * @throws RemoteException
     */
    EDGERemote createEDGE() throws RemoteException;

    /**
     * Unbinds the RMI Registry and shutdown the server.
     *
     * @throws RemoteException
     */
    public void exit() throws RemoteException;

    /**
     * Kill brutally the server.
     *
     * @throws RemoteException
     */
    public void forceExit() throws RemoteException;

    /**
     * Returns a remote input stream for the streaming of a file.
     *
     * @param f
     * @return
     * @throws RemoteException
     */
    public InputStream getInputStream(File f) throws IOException, RemoteException;

    /**
     * Returns a remote output stream for the streaming of a file.
     *
     * @param f
     * @return
     * @throws RemoteException
     */
    public OutputStream getOutputStream(File f) throws IOException, RemoteException;

//    /**
//     * Returns a remote input stream in order to stream an object.
//     *
//     * @return
//     * @throws RemoteException
//     */
//    public InputStream getInputStream() throws IOException, RemoteException;
//
//    /**
//     * Returns a remote output stream in order to stream an object.
//     *
//     * @return
//     * @throws RemoteException
//     */
//    public OutputStream getOutputStream() throws IOException, RemoteException;

}
