/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.rmi.communication;

import java.io.IOException;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese <riccardo.zese@unife.it>
 */
public class RMIOutputStreamImpl implements RMIOutputStream {

    private OutputStream out;
    private RMIPipe pipe;

    public RMIOutputStreamImpl(OutputStream out) throws RemoteException {
        this.out = out;
        //this.pipe = new RMIPipe(out);
        UnicastRemoteObject.exportObject(this, 1099); // è necessario????
    }

    RMIOutputStreamImpl() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void write(int b) throws IOException, RemoteException {
        out.write(b);
    }

    public void write(byte[] b, int off, int len) throws IOException, RemoteException {
        out.write(b, off, len);
    }

    public void close() throws IOException, RemoteException {
        out.close();
    }
    
    
}
