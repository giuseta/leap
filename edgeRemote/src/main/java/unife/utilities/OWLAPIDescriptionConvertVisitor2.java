/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.utilities;

import java.lang.reflect.Field;
import java.util.Stack;
import org.dllearner.core.owl.Description;
import org.dllearner.core.owl.Negation;
import org.dllearner.utilities.owl.OWLAPIDescriptionConvertVisitor;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;

/**
 * Converter from DL-Learner descriptions to OWL API descriptions based on the
 * visitor pattern.
 *
 * @author 
 *
 *
 */
public class OWLAPIDescriptionConvertVisitor2 extends OWLAPIDescriptionConvertVisitor {

    // private OWLDescription description;
    private Stack<OWLClassExpression> stack;

    private OWLDataFactory factory = OWLManager.createOWLOntologyManager().getOWLDataFactory();
    
    	/**
	 * Converts a DL-Learner description into an OWL API decription.
	 * @param description DL-Learner description.
	 * @return Corresponding OWL API description.
	 */
	public static OWLClassExpression getOWLClassExpression(Description description) {
		OWLAPIDescriptionConvertVisitor2 converter = new OWLAPIDescriptionConvertVisitor2();
		description.accept(converter);
		return converter.getOWLClassExpression();
	}

    /* (non-Javadoc)
     * @see org.dllearner.core.owl.DescriptionVisitor#visit(org.dllearner.core.owl.Negation)
     */
    @Override
    public void visit(Negation description) {
        description.getChild(0).accept(this);
        try {
            //Field privateField = super.getClass().getDeclaredField("stack");
            Field privateField = this.getClass().getSuperclass().getDeclaredField("stack");
            privateField.setAccessible(true);
            //stack = (Stack<OWLClassExpression>) privateField.get((OWLDescriptionConvertVisitor) super);
            stack = (Stack<OWLClassExpression>) privateField.get(this);
        } catch (Exception e) {
            System.exit(-1);
        }
        OWLClassExpression d = stack.pop();
        if (d.isOWLThing()) {
            stack.push(factory.getOWLNothing());
        } else if (d.isOWLNothing()) {
            stack.push(factory.getOWLThing());
        } else {
            stack.push(factory.getOWLObjectComplementOf(d));
        }
    }
}
