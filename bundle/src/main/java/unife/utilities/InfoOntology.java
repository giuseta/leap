/**
 * This file is part of LEAP.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * LEAP is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * LEAP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package unife.utilities;

import java.io.File;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

/**
 * Utility class that returns the number of individuals and the number of axioms
 * contained into a knowledge base
 *
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class InfoOntology {

    /**
     * @param args the command line arguments
     * @throws org.semanticweb.owlapi.model.OWLOntologyCreationException
     */
    public static void main(String[] args) throws OWLOntologyCreationException {
        if (args.length != 1) {
            System.out.println("ERROR: wrong number of arguments!");
            System.out.println("Usage: info_ontology.sh <ontology_filaname>");
            System.exit(1);
        }
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File(args[0]));
        int numIndividuals = ontology.getIndividualsInSignature(true).size();
        System.out.println("Number of individuals: " + numIndividuals);
        // migliorare ques'ultimo comando per poter contare anche gli assiomi importati
        int numAxioms = ontology.getLogicalAxiomCount();
        System.out.println("Number of logical axioms: " + numAxioms);
        numAxioms = ontology.getAxiomCount();
        System.out.println("Number of axioms: " + numAxioms);
    }

}
