/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.bundle;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.semanticweb.owlapi.model.OWLAxiom;
import net.sf.javabdd.BDD;

/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese <riccardo.zese@unife.it>
 */
public class QueryResult {

    private BigDecimal queryProbability;
    private BDD bdd;
    private Set<Set<OWLAxiom>> explanations;
    private List<OWLAxiom> probAxioms;
    private List<BigDecimal> probOfAxioms;

    private int numberOfExplanations;

    public QueryResult() {
        explanations = new HashSet<>();
        numberOfExplanations = 0;
    }

    /**
     * @return the explanations
     */
    public Set<Set<OWLAxiom>> getExplanations() {
        return explanations;
    }

    /**
     * @param explanations the explanations to set
     */
    public void setExplanations(Set<Set<OWLAxiom>> explanations) {
        this.explanations = explanations;
        numberOfExplanations = explanations.size();
    }

    /**
     * @return the probAxioms
     */
    public List<OWLAxiom> getProbAxioms() {
        return probAxioms;
    }

    /**
     * @param probAxioms the probAxioms to set
     */
    public void setProbAxioms(List<OWLAxiom> probAxioms) {
        this.probAxioms = probAxioms;
    }

    /**
     * @return the probOfAxioms
     */
    public List<BigDecimal> getProbOfAxioms() {
        return probOfAxioms;
    }

    /**
     * @param probOfAxioms the probOfAxioms to set
     */
    public void setProbOfAxioms(List<BigDecimal> probOfAxioms) {
        this.probOfAxioms = probOfAxioms;
    }

    /**
     * @return the queryProbability
     */
    public BigDecimal getQueryProbability() {
        return queryProbability;
    }

    /**
     * @param probability the queryProbability to set
     */
    public void setQueryProbability(BigDecimal probability) {
        this.queryProbability = probability;
    }

    public void merge(QueryResult qr) {
        explanations.addAll(qr.getExplanations());
    }

    /**
     * @return the number of explanations
     */
    int getNumberOfExplanations() {
        return numberOfExplanations;
    }

    /**
     * @return the bdd
     */
    public BDD getBDD() {
        return bdd;
    }

    /**
     * @param bdd the bdd to set
     */
    public void setBDD(BDD bdd) {
        this.bdd = bdd;
    }

    @Override
    public String toString() {
        return "N. of Explanations: " + numberOfExplanations
                + " | Probability of the query: " + queryProbability;
    }
}
