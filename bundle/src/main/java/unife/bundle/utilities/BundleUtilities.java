/**
 * This file is part of LEAP.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * LEAP is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * LEAP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package unife.bundle.utilities;

import com.clarkparsia.owlapi.explanation.io.manchester.TextBlockWriter;
import org.semanticweb.owlapi.manchestersyntax.renderer.ManchesterOWLSyntaxObjectRenderer;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.AddImport;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLImportsDeclaration;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;
import uk.ac.manchester.cs.owl.owlapi.OWLAnnotationPropertyImpl;
import unife.bundle.logging.BundleLoggerFactory;
import unife.math.utilities.MathUtilities;

/**
 * Class containing some useful methods.
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota
 * <giuseta@gmail.com>
 */
public class BundleUtilities {

    public static final OWLAnnotationProperty PROBABILISTIC_ANNOTATION_PROPERTY = new OWLAnnotationPropertyImpl(IRI.create("https://sites.google.com/a/unife.it/ml/disponte#probability"));

    private static final Logger logger = Logger.getLogger(BundleUtilities.class.getName(), new BundleLoggerFactory());

    /**
     * This method converts an OWL object to Manchester syntax.
     *
     * @param owlObject
     * @return
     */
    public static String getManchesterSyntaxString(OWLObject owlObject) {
        StringWriter stringWriter = new StringWriter();
        TextBlockWriter printWriter = new TextBlockWriter(stringWriter);
        ManchesterOWLSyntaxObjectRenderer manch = new ManchesterOWLSyntaxObjectRenderer(printWriter, new SimpleShortFormProvider());
        owlObject.accept(manch);
        return stringWriter.toString();
    }

    /**
     * It returns a copy of a given OWL ontology
     *
     * @param manager
     * @param ontology
     * @return
     * @throws OWLOntologyCreationException
     */
    public static OWLOntology copyOntology(OWLOntologyManager manager, OWLOntology ontology) throws OWLOntologyCreationException {
        OWLOntology ontologyCopy = manager.createOntology();
        manager.addAxioms(ontologyCopy, ontology.getAxioms());

        for (OWLImportsDeclaration d : ontology.getImportsDeclarations()) {
            manager.applyChange(new AddImport(ontologyCopy, d));
        }

        return ontologyCopy;
    }

    /**
     * It returns a copy of a given OWL ontology
     *
     * @param ontology
     * @return
     * @throws OWLOntologyCreationException
     */
    public static OWLOntology copyOntology(OWLOntology ontology) throws OWLOntologyCreationException {
        return copyOntology(ontology.getOWLOntologyManager(), ontology);
    }

    /**
     * It returns an ontology containing only the certain axioms of a given
     * ontology.
     *
     * @param manager
     * @param ontology
     * @return
     * @throws OWLOntologyCreationException
     */
    public static OWLOntology copyCertainOntology(OWLOntologyManager manager, OWLOntology ontology) throws OWLOntologyCreationException {
        OWLOntology ontologyCopy = BundleUtilities.copyOntology(manager, ontology);
        // probabilistic axioms to remove
        Set<OWLAxiom> probAxioms = BundleUtilities.getProbabilisticAxioms(ontologyCopy);
        manager.removeAxioms(ontologyCopy, probAxioms);
        return ontologyCopy;
    }

    /**
     * Returns a copy of a given ontology containing only the probabilistic
     * axioms
     *
     * @param manager
     * @param ontology
     * @return The copy of ontology with only the probabilistic axioms
     * @throws OWLOntologyCreationException
     */
    public static OWLOntology copyProbabilisticOntology(OWLOntologyManager manager,
            OWLOntology ontology) throws OWLOntologyCreationException {
        Set<OWLAxiom> probAxioms = BundleUtilities.getProbabilisticAxioms(ontology);
        OWLOntology ontologyCopy = manager.createOntology();
        manager.addAxioms(ontologyCopy, probAxioms);
        return ontologyCopy;
    }

    /**
     * It returns the set of the probabilistic axioms.
     *
     * @param ontology
     * @return
     */
    public static Set<OWLAxiom> getProbabilisticAxioms(OWLOntology ontology) {
        Set<OWLAxiom> probAxioms = new HashSet<OWLAxiom>();
        for (OWLAxiom axiom : ontology.getAxioms()) {
            if (axiom.getAnnotations(PROBABILISTIC_ANNOTATION_PROPERTY).size() > 0) {
                probAxioms.add(axiom);
            }
        }
        return probAxioms;
    }

    /**
     * It converts the input time string into the corresponding integer value in
     * milliseconds.
     *
     * @param stringTime The input string in the format -h-m-s-ms, where '-' are
     * the placeholder for the integer values of hours (h), minutes (m), seconds
     * (s) and milliseconds (ms). It is not mandatory that the string contains
     * all the four values, but the order must be followed.
     * @return the integer value in milliseconds of the time specified in
     * stringTime.
     */
    public static int convertTimeValue(String stringTime) {

        int h = 0, m = 0, s = 0;

        if (stringTime != null) {
            if (stringTime.matches("([0-9]+h){0,1}([0-9]+m){0,1}([0-9]+s){0,1}")) {
                String[] div = stringTime.split("\\d+");
                String[] val = stringTime.split("\\D+");

                try {

                    for (int i = 0; i < val.length; i++) {
                        if (div[i + 1].equals("h")) {
                            h = Integer.parseInt(val[i]);
                        } else if (div[i + 1].equals("m")) {
                            m = Integer.parseInt(val[i]);
                        } else if (div[i + 1].equals("s")) {
                            s = Integer.parseInt(val[i]);
                        }
                    }

                    return ((((h * 60) + m) * 60) + s);

                } catch (Exception e) {
                    System.err.println("Incorrect format for maximum execution time. Time setted to 0!");
                    return 0;
                }
            } else {
                System.err.println("Incorrect format for maximum execution time. Time setted to 0!");
                return 0;
            }
        } else {
            return 0;
        }

    }

    /**
     * Takes an ontology and returns a Map containing all the probabilistic
     * axioms with their probabilities.
     *
     * @param axioms
     * @param showAll
     * @param randomize
     * @param randomizeAll
     * @param accuracy
     * @param randomSeed
     * @param log
     * @return
     */
    public static Map<OWLAxiom, BigDecimal> createPMap(SortedSet<OWLAxiom> axioms,
            boolean showAll, boolean randomize, boolean randomizeAll, int accuracy,
            Integer randomSeed) {

        logger.debug("Preparing Probability Map...");
        System.out.println("Preparing Probability Map...");

        Map<OWLAxiom, BigDecimal> pMap = new HashMap<>();

        Random probGenerator = new Random();

        if (randomize) {
            if (randomSeed != null) {
                probGenerator.setSeed(randomSeed);
                logger.debug("Random Seed set to: " + randomSeed);
            }
        }

        for (OWLAxiom axiom : axioms) {
            List<BigDecimal> probList = new ArrayList<>();
            String axiomName = getManchesterSyntaxString(axiom);

            if (randomizeAll) {

                probList.add(MathUtilities.getBigDecimal(probGenerator.nextDouble(), accuracy));

            } else {

                for (OWLAnnotation annotation : axiom.getAnnotations(BundleUtilities.PROBABILISTIC_ANNOTATION_PROPERTY)) {

                    // metodo per aggiungere coppia assioma/probabilita alla Map
                    if (annotation.getValue() != null) {

                        BigDecimal annProbability;
                        if (randomize) {
                            annProbability = MathUtilities.getBigDecimal(probGenerator.nextDouble(), accuracy);
                            probList.add(annProbability);
                        } else {
                            String annotationStr = annotation.getValue().toString().trim();
                            annotationStr = annotationStr.replaceAll("\"", "");
                            if (annotationStr.contains("^^")) {
                                annotationStr = annotationStr.substring(0, annotationStr.indexOf("^^"));
                            }
                            annProbability = MathUtilities.getBigDecimal(annotationStr, accuracy);
                            probList.add(annProbability);

                        }
                        if (showAll) {
                            System.out.print(axiomName);
                            String str = " => " + annProbability;
                            System.out.println(str);
                            logger.info(axiomName + str);
                        }
                    }
                }
            }
            if (probList.size() > 0) {
                OWLAxiom pMapAxiom = axiom.getAxiomWithoutAnnotations();
                BigDecimal varProbTemp = MathUtilities.getBigDecimal("0", accuracy);
                if (pMap.containsKey(pMapAxiom)) {
                    varProbTemp = pMap.get(pMapAxiom);
                }
                for (BigDecimal ithProb : probList) {
                    BigDecimal mul = varProbTemp.multiply(ithProb).setScale(accuracy, RoundingMode.HALF_UP);
                    varProbTemp = varProbTemp.add(ithProb).subtract(mul);
                }
                pMap.put(pMapAxiom, varProbTemp);
            }

        }

        if (randomizeAll) {
            System.out.println("Created " + axioms.size() + " probabilistic axiom");
            logger.info("Created " + axioms.size() + " probabilistic axiom");
        }

        if (pMap.size() > 0) {
             System.out.println("Probability Map computed. Size: " + pMap.size());
            logger.info("Probability Map computed. Size: " + pMap.size());
            return pMap;
        } else {
             System.out.println("Probability Map computed. Size: " + pMap.size());
            logger.info("Probability Map is empty");
            return null;
        }

    }
}
