/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.bundle.monitor;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;
import unife.bundle.Bundle;
import unife.bundle.logging.BundleLoggerFactory;
import unife.bundle.renderer.LogManchesterSyntaxExplanationRenderer;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota <giuseta@gmail.com>
 */
public class LogRendererTimeExplanationProgressMonitor implements BundleRendererExplanationProgressMonitor, TimeMonitor {
    
    //Logger.getLogger(EDGE.class.getName());
    
    
    private LogManchesterSyntaxExplanationRenderer rend;
    private OWLAxiom axiom;
    private Set<Set<OWLAxiom>> setExplanations;
    private Logger logger;
    private boolean stop = false;
    private int execTime;
    private Thread t;

    public LogRendererTimeExplanationProgressMonitor(OWLAxiom axiom) {
        this.axiom = axiom;
        this.logger = Logger.getLogger(Bundle.class.getName(), new BundleLoggerFactory());
        rend =  new LogManchesterSyntaxExplanationRenderer();
        this.execTime = 0;

        setExplanations = new HashSet<>();
        try {
            rend.startRendering(logger);

        } catch (OWLException | IOException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }
    }

    

    @Override
    public void foundExplanation(Set<OWLAxiom> axioms) {

        if (!setExplanations.contains(axioms)) {
            setExplanations.add(axioms);
            try {
                rend.render(axiom, Collections.singleton(axioms));
            } catch (IOException | OWLException e) {
                throw new RuntimeException("Error rendering explanation: " + e);
            }
        }
    }

    public void startMonitoring() {
        if (execTime > 0) {
            t = new Thread(new TimerChecker());
            t.start();
        }
    }
    
    @Override
    public void setParamAndStart(int time) {
        setExecTime(time);
        startMonitoring();
    }
    
    public void setExecTime(int time) {
        this.execTime = time * 1000;
    }
    
    public int getExecTime() {
        return execTime;
    }
    
    @Override
    public void stopMonitoring() {
        if (t != null && t.isAlive())
            t.stop();
    }

    @Override
    public boolean isCancelled() {
//            if (execTimer == null)
//                return false;
//            else
//                //System.out.println("time " + execTimer.getElapsed() + " " + maxTime);
//                return (execTimer.getElapsed() > maxTime);
        return stop;
    }

    @Override
    public void foundAllExplanations() {
        try {
            rend.endRendering();
        } catch (OWLException | IOException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }
    }

    @Override
    public void foundNoExplanations() {
        try {
            rend.render(axiom, Collections.<Set<OWLAxiom>>emptySet());
            rend.endRendering();
        } catch (OWLException | IOException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }
    }

    
    @Override
    public void write(String s) {
        rend.write(s);
    }

    @Override
    public void writeln(String s) {
        rend.writeln(s);
    }
    
    private class TimerChecker implements Runnable
    {
        @Override
        public void run() {
            try {
                Thread.sleep(getExecTime());
                stop = true;
                
            } catch (InterruptedException ex) {
                throw new RuntimeException("Time monitors ended abnormally: " + ex);
            } catch (Exception ex) {
                throw new RuntimeException("Time monitors ended abnormally: " + ex);
            }
        
        }
   }

}
