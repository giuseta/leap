/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.bundle.logging;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota <giuseta@gmail.com>
 */
public class BundleLogger extends Logger {
    
    private static final String FQCN = BundleLogger.class.getName();

    public BundleLogger(String name) {
        super(name);
    }

    private String[] splitOnRows(Object x) {
        String string;
        if (x instanceof String) {
            string = (String) x;
        } else {
            string = x.toString();
        }

        return string.split("\n");

    }

    /**
     * @inheritDoc
     */
    @Override
    public void info(Object message) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.INFO, s, null);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void info(Object message, Throwable trbObj) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.INFO, s, null);
        }
        super.log(FQCN, Level.INFO, "", trbObj);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void error(Object message) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.ERROR, s, null);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void error(Object message, Throwable trbObj) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.ERROR, s, null);
        }
        super.log(FQCN, Level.ERROR, "", trbObj);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void warn(Object message) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.WARN, s, null);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void warn(Object message, Throwable trbObj) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.WARN, s, null);
        }
        super.log(FQCN, Level.WARN, "", trbObj);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void debug(Object message) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.DEBUG, s, null);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void debug(Object message, Throwable trbObj) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.DEBUG, s, null);
        }
        super.log(FQCN, Level.DEBUG, "", trbObj);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void fatal(Object message) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.FATAL, s, null);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void fatal(Object message, Throwable trbObj) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.FATAL, s, null);
        }
        super.log(FQCN, Level.FATAL, "", trbObj);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void trace(Object message) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.TRACE, s, null);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void trace(Object message, Throwable trbObj) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, Level.TRACE, s, null);
        }
        super.log(FQCN, Level.TRACE, "", trbObj);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void log(Priority priority, Object message) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, priority, s, null);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void log(Priority priority, Object message, Throwable t) {
        String[] strings = splitOnRows(message);
        for (String s : strings) {
            super.log(FQCN, priority, s, null);
        }
        super.log(FQCN, priority, "", t);
    }

}
